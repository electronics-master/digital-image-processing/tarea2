#ifndef __CONTROLLER_HPP__
#define __CONTROLLER_HPP__

class IController
{
public:
  static IController * newController ();
  virtual void process (float rotation[4][4]) = 0;
};

#endif //__CONTROLLER_HPP__
