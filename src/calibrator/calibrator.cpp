// Used for image manipulation
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//Used for interaction with the user
#include <boost/program_options.hpp>

// Standard Library
#include <fstream>
#include <iostream>
#include <string>

namespace po = boost::program_options;

#define DEFAULT_MAX_FRAMES 20
#define DEFAULT_BOARD_WIDTH 5
#define DEFAULT_BOARD_HEIGHT 4
#define DEFAULT_SQUARE_SIZE 40
#define DEFAULT_CALIBRATION_NAME "calibration.yml"
#define DEFAULT_TOL 1.0

enum {
    CALIBRATING,
    CALIBRATED
};

void saveCameraParams(const std::string& filename,
                      cv::Mat &cameraMatrix,
                      cv::Mat &distCoeffs) {
    cv::FileStorage file(filename, cv::FileStorage::WRITE);
    file << "cameraMatrix" << cameraMatrix;
    file << "distCoeffs" << distCoeffs;
}

void loadCameraParams(const std::string& filename,
                      cv::Mat &cameraMatrix,
                      cv::Mat &distCoeffs) {
    cv::FileStorage file(filename, cv::FileStorage::READ);

    file["cameraMatrix"] >> cameraMatrix;
    file["distCoeffs"] >> distCoeffs;
}

bool calibrateCamera(cv::Size &boardSize, int square_side,
                     std::vector<std::vector<cv::Point2f> > &imagePoints,
                     cv::Mat &cameraMatrix, cv::Mat &distCoeffs,
                     cv::Size &imageSize,
                     int tol) {
    std::vector<std::vector<cv::Point3f> > objectPoints(1);
    std::vector<cv::Mat> rvecs, tvecs;
    double rms;
    int i, j;

    distCoeffs = cv::Mat::zeros(8, 1, CV_64F);

    // Mathematically gets expected matrix pattern
    for( i = 0; i < boardSize.height; ++i ) {
        for( j = 0; j < boardSize.width; ++j ) {
            objectPoints[0].push_back(cv::Point3f(float( j*square_side ),
                                                  float( i*square_side ), 0));
        }
    }

    // Replicates object points to match image poitns size
    objectPoints.resize(imagePoints.size(), objectPoints[0]);

    // Initial camera matrix approximation
    cameraMatrix = initCameraMatrix2D(objectPoints, imagePoints, imageSize);

    std::cout << "Calibrating\n";

    // Get camera matrix and distortion coefficients
    rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix,
                          distCoeffs, rvecs, tvecs);

    std::cout << "Current rms: " << rms << "\n";

    return rms < tol;
}


int process(const std::string& input, unsigned int maxFrames,
            cv::Size boardSize,
            int squareSide, double tol, const std::string& in_filename,
            const std::string& out_filename) {
    std::vector<std::vector<cv::Point2f> > imagePoints;
    std::vector<cv::Point2f> pointBuf;
    const cv::Scalar RED(0,0,255), GREEN(0,255,0);
    cv::Mat frame, cameraMatrix, distCoeffs;
    cv::VideoCapture inputCapture;
    cv::Size imageSize;
    bool showUndistorted = false;
    int status = CALIBRATING;

    if(!in_filename.empty()) {
        loadCameraParams(in_filename, cameraMatrix, distCoeffs);
        status = CALIBRATED;
    }

    if(!inputCapture.open(input)) {
        std::cerr << "Unable to open input" << "\n";
        return 1;
    }

    std::cout <<
              "Press u to change between distorted and undistorted view\n"
              "Press q to exit" <<
              std::endl;

    for(;;) {
        inputCapture >> frame;

        if(!frame.empty()) {
            imageSize = frame.size();
        }

        if( (status == CALIBRATING) && (imagePoints.size() >= maxFrames)) {
            if(calibrateCamera(boardSize, squareSide,
                               imagePoints,
                               cameraMatrix, distCoeffs,
                               imageSize, tol)) {
                saveCameraParams(out_filename, cameraMatrix, distCoeffs);
                status = CALIBRATED;
            } else {
                status = CALIBRATING;
                maxFrames *= 1.1;
            }
        }

        if (frame.empty()) {
            if (status != CALIBRATED) {
                std::cout <<
                          "Max number of frames not reached, calibrating with current data\n";
                calibrateCamera(boardSize, squareSide,
                                imagePoints,
                                cameraMatrix, distCoeffs,
                                imageSize, tol);
                saveCameraParams(out_filename, cameraMatrix, distCoeffs);
            }
            break;
        }

        bool found = findCirclesGrid( frame, boardSize, pointBuf );

        if (found) {
            imagePoints.push_back(pointBuf);

            drawChessboardCorners( frame, boardSize, cv::Mat(pointBuf), found );
        }



        // Output text
        std::string msg;
        int baseLine = 0;

        if( status == CALIBRATING ) {
            msg = "100/100";
            if(showUndistorted) {
                msg = cv::format( "%d/%d Undist", static_cast<int>(imagePoints.size()),
                                  maxFrames );
            } else {
                msg = cv::format( "%d/%d", static_cast<int>(imagePoints.size()), maxFrames );
            }
        }

        else if( status == CALIBRATED ) {
            if(showUndistorted) {
                msg = "Undistorted Calibrated";
            } else {
                msg = "Distorted Calibrated";
            }
        } else {
            msg = "Unknown state";
        }

        cv::Size textSize = cv::getTextSize(msg, 1, 1, 1, &baseLine);
        cv::Point textOrigin(frame.cols - 2*textSize.width - 10,
                             frame.rows - 2*baseLine - 10);

        putText( frame, msg, textOrigin, 1, 1, status == CALIBRATED ?  GREEN : RED);

        cv::Mat temp;
        temp = frame.clone();

        if( status == CALIBRATED ) {
            undistort(temp, frame, cameraMatrix, distCoeffs);
        }

        if (!showUndistorted) {
            imshow("Image View", frame);
        } else {
            imshow("Image View", temp);
        }
        auto key = static_cast<char>(cv::waitKey(50));

        if( key  == 'q' ) {
            break;
        }

        if( key == 'u' && status == CALIBRATED ) {
            showUndistorted = !showUndistorted;
        }
    }
    return 0;
}

int main(int ac, char* av[]) {
    std::string input, in_filename, out_filename;
    cv::Size boardSize(DEFAULT_BOARD_WIDTH, DEFAULT_BOARD_HEIGHT);
    unsigned int maxFrames = DEFAULT_MAX_FRAMES, squareSize = DEFAULT_SQUARE_SIZE;
    double tol = DEFAULT_TOL;

    try {
        po::options_description desc("Allowed Options");
        desc.add_options()
        ("help", "Produce help message")
        ("input", po::value<std::string>(),
         "Required, Video device, file sequence or video to calibrate from")
        ("max_frames", po::value<int>()->default_value(DEFAULT_MAX_FRAMES),
         "Max number of frames to calibrate from")
        ("board_width", po::value<int>()->default_value(DEFAULT_BOARD_WIDTH),
         "Board width in number of circles")
        ("board_height", po::value<int>()->default_value(DEFAULT_BOARD_HEIGHT),
         "Board height in number of circles")
        ("square_size", po::value<int>()->default_value(DEFAULT_SQUARE_SIZE),
         "Square size for each circle in the pattern in mm")
        ("tol", po::value<double>()->default_value(DEFAULT_TOL),
         "Tolerance for calibration calculation")
        ("calibrate_parameters", po::value<std::string>(),
         "Calibration parameters to load data from or to save to")
        ("in_filename", po::value<std::string>(),
         "Filename to load calibration parameters from."
         "Should be either an .xml or a .yml"
         "containing a 3x3 cameraMatrix and a 5x1 distCoeffs matrix")
        ("out_filename", po::value<std::string>()->default_value(
             DEFAULT_CALIBRATION_NAME),
         "Filename for save calibration parameters to. Can be either xml or yml")
        ;

        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, desc), vm);
        po::notify(vm);

        if(vm.count("help") != 0u) {
            std::cout << desc << "\n";
            return 0;
        }

        if(vm.count("max_frames") != 0u) {
            maxFrames = vm["max_frames"].as<int>();
        }

        if(vm.count("board_width") != 0u) {
            boardSize.width = vm["board_width"].as<int>();
        }

        if(vm.count("board_height") != 0u) {
            boardSize.height = vm["board_height"].as<int>();
        }

        if(vm.count("square_size") != 0u) {
            squareSize = vm["square_size"].as<int>();
        }

        if(vm.count("tol") != 0u) {
            tol = vm["tol"].as<double>();
        }

        if(vm.count("in_filename") != 0u) {
            in_filename = vm["in_filename"].as<std::string>();
        }

        if(vm.count("out_filename") != 0u) {
            out_filename = vm["out_filename"].as<std::string>();
        }

        if(vm.count("input") != 0u) {
            input = vm["input"].as<std::string>();
        } else {
            std::cout << "Input is required" << "\n";
            std::cout << desc << "\n";
            return 1;
        }

        return process(input, maxFrames, boardSize, squareSize, tol, in_filename,
                       out_filename);

    } catch(std::exception& e) {
        std::cerr << "error: " << e.what() << "\n";
        return 1;
    } catch(...) {
        std::cerr << "Exception of unknown type!\n";
    }

    return 0;

}
