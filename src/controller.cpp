#include "controller.hpp"

// Used for image manipulation
#include <cmath>
#include <opencv2/aruco.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <cmath>
#include <iostream>
#include <memory>

#include <cmath>       /* exp */

#define ARUCO_LENGHT 0.1
#define CALIBRATION_NAME "calibration.yml"

#define MATRIX_DELAY 1

#define DINOSAUR_WIDTH 16.0
#define DINOSAUR_HEIGHT 16.0

#define N 4

const float ident[N][N] = {{1, 0, 0, 0},
    {0, 1, 0, 0},
    {0, 0, 1, 0},
    {0, 0, 0, 1}
};

static void multiply(const float mat1[][N],
                     const float mat2[][N],
                     float res[][N]) {
    int i, j, k;

    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            res[i][j] = 0;
            for (k = 0; k < N; k++) {
                res[i][j] += mat1[i][k] *
                             mat2[k][j];
            }
        }
    }
}

static void add(const float mat1[][N],
                const float mat2[][N],
                float res[][N]) {
    int i, j;

    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            res[i][j] = mat1[i][j] + mat2[i][j];
        }
    }
}

static void add(const float scalar,
                const float mat[][N],
                float res[][N]) {
    int i, j;

    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            res[i][j] = scalar * mat[i][j];
        }
    }
}

static void average_matrices(std::vector<std::shared_ptr<float[]>>
                             &matrix_vector,
                             const float in_mat[N][N],
                             float out_mat[N][N]) {
    std::shared_ptr<float[]> new_mat(new float[N * N]);
    unsigned int i, j, k;

    for(i = 0; i < N; i ++) {
        for(j = 0; j < N; j ++) {
            new_mat[i * N + j] = in_mat[i][j];
        }
    }

    // Add newest matrix at the end
    matrix_vector.push_back(new_mat);

    // Remove oldest vector
    if(matrix_vector.size() > MATRIX_DELAY) {
        matrix_vector.erase(matrix_vector.begin());
    }

    for(i = 0; i < N; i ++) {
        for(j = 0; j < N; j ++) {
            out_mat[i][j] = 0;
            for(k = 0; k < matrix_vector.size(); k++) {
                out_mat[i][j] += matrix_vector[k][i * N + j];
            }
            out_mat[i][j] /= matrix_vector.size();
        }
    }
}

static void reset_matrix(float mat[][N]) {
    memcpy(mat, ident, sizeof(ident));
}

class Controller : public IController {
  public:

    Controller() {
        cv::FileStorage file(CALIBRATION_NAME, cv::FileStorage::READ);

        file["cameraMatrix"] >> this->cameraMatrix;
        file["distCoeffs"] >> this->distCoeffs;

        f_x = cameraMatrix.at<double>(0, 0);
        f_y = cameraMatrix.at<double>(1, 1);
        c_x = cameraMatrix.at<double>(0, 2);
        c_y = cameraMatrix.at<double>(1, 2);

    }
    void process (float M[4][4]) override {
        cv::Mat frame;
        std::vector<cv::Point2f> pointBuf;

        float centerX = -DINOSAUR_WIDTH / 2;
        float centerY = -DINOSAUR_HEIGHT / 2;
        float centerZ = 0;

        if(!inputCapture.isOpened()) {
            if(!inputCapture.open(0)) {
                std::cerr << "Unable to open input\n";
                return;
            }
        }

        inputCapture >> frame;

        if (!frame.empty()) {
            cv::aruco::detectMarkers(frame, dictionary, markerCorners, markerIds,
                                     parameters, rejectedCandidates,
                                     cameraMatrix, distCoeffs);
            cv::aruco::estimatePoseSingleMarkers(markerCorners, ARUCO_LENGHT,
                                                 cameraMatrix, distCoeffs,
                                                 rvecs, tvecs);

            for (int i = 0; i < rvecs.size(); ++i) {
                auto rvec = rvecs[i];
                auto tvec = tvecs[i];
                cv::aruco::drawAxis(frame, cameraMatrix, distCoeffs, rvec, tvec, 0.1);
            }

            if (!rvecs.empty()) {
                cv::Vec3d rvec = rvecs[0], tvec = tvecs[0];

                float tX = tvec[0] * 50;
                float tY = tvec[1] * 50;
                float tZ = tvec[2] * 10;

                // Translation matrix
                reset_matrix(T);

                T[0][3] = centerX + tX;
                T[1][3] = centerY - tY;
                T[2][3] = -pow(1.25, centerZ + tZ); // Amplify movement in Z

                // RVecs are described by Rodrigues Vectors rather than Eueler
                cv::Mat rotation_mat(3, 3, CV_32F);
                cv::Rodrigues(rvec, rotation_mat);

                reset_matrix(Rzyx);
                for (int i=0; i < 3; i++) {
                    for (int j=0; j < 3; j++) {
                        Rzyx[i][j] = rotation_mat.at<double>(j, i);
                    }
                }
                multiply(Rzyx, T, M);

                // Used to smooth out the movement and reduce noise
                average_matrices(old_matrices, M, M);
            }

            imshow("Source View", frame);
            cv::waitKey(1);
        }

    }

  private:
    cv::VideoCapture inputCapture;
    cv::Mat cameraMatrix, distCoeffs;

    std::vector<int> markerIds;
    std::vector<std::vector<cv::Point2f>> markerCorners, rejectedCandidates;
    cv::Ptr<cv::aruco::DetectorParameters> parameters =
        cv::aruco::DetectorParameters::create();
    cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(
                cv::aruco::DICT_6X6_100);


    std::vector<cv::Vec3d> rvecs, tvecs;

    std::vector<std::shared_ptr<float[]>> old_matrices;

    // Camera Matrix parameters
    float f_x;
    float f_y;
    float c_x;
    float c_y;

    // Geometric transformation matrices
    float Rx[N][N] {};
    float Ry[N][N] {};
    float Rz[N][N] {};
    float Ryx[N][N] {};
    float Rzyx[N][N] {};
    float T[N][N] {};
};

IController *IController::newController () {
    return new Controller ();
}
