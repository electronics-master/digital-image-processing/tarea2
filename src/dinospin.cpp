/* Copyright (c) Mark J. Kilgard, 1994.  */

/* This program is freely distributable without licensing fees
   and is provided without guarantee or warrantee expressed or
   implied. This program is -not- in the public domain. */

/* New GLUT 3.0 glutGetModifiers() functionality used to make Shift-Left
   mouse scale the dinosaur's size. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h> /* for cos(), sin(), and sqrt() */
#include <GL/glut.h>

#include "controller.hpp"

#define BODY_WIDTH 3.0
#define __stdcall

typedef enum {
    RESERVED,
    BODY_SIDE,
    BODY_EDGE,
    BODY_WHOLE,
    ARM_SIDE,
    ARM_EDGE,
    ARM_WHOLE,
    LEG_SIDE,
    LEG_EDGE,
    LEG_WHOLE,
    EYE_SIDE,
    EYE_EDGE,
    EYE_WHOLE,
    DINOSAUR
} displayLists;

/* *INDENT-OFF* */
GLfloat body[][2] = {{0, 3}, {1, 1}, {5, 1}, {8, 4}, {10, 4}, {11, 5}, {11, 11.5}, {13, 12}, {13, 13}, {10, 13.5}, {13, 14}, {13, 15}, {11, 16}, {8, 16}, {7, 15}, {7, 13}, {8, 12}, {7, 11}, {6, 6}, {4, 3}, {3, 2}, {1, 2}};
GLfloat arm[][2] = {{8, 10}, {9, 9}, {10, 9}, {13, 8}, {14, 9}, {16, 9}, {15, 9.5}, {16, 10}, {15, 10}, {15.5, 11}, {14.5, 10}, {14, 11}, {14, 10}, {13, 9}, {11, 11}, {9, 11}};
GLfloat leg[][2] = {{8, 6}, {8, 4}, {9, 3}, {9, 2}, {8, 1}, {8, 0.5}, {9, 0}, {12, 0}, {10, 1}, {10, 2}, {12, 4}, {11, 6}, {10, 7}, {9, 7}};
GLfloat eye[][2] = {{8.75, 15}, {9, 14.7}, {9.6, 14.7}, {10.1, 15}, {9.6, 15.25}, {9, 15.25}};
GLfloat lightZeroPosition[] = {10.0, 4.0, 10.0, 1.0};
GLfloat lightZeroColor[] = {0.8, 1.0, 0.8, 1.0}; /* green-tinted */
GLfloat lightOnePosition[] = {-1.0, -2.0, 1.0, 0.0};
GLfloat lightOneColor[] = {0.6, 0.3, 0.2, 1.0}; /* red-tinted */
GLfloat skinColor[] = {0.1, 1.0, 0.1, 1.0}, eyeColor[] = {1.0, 0.2, 0.2, 1.0};
/* *INDENT-ON* */

/* Project state */
IController *controller;

static void
extrudeSolidFromPolygon(GLfloat data[][2], unsigned int dataSize,
                        GLdouble thickness, GLuint side, GLuint edge, GLuint whole) {
    static GLUtriangulatorObj *tobj = NULL;
    GLdouble vertex[3], dx, dy, len;
    int i;
    int count = (int)(dataSize / (2 * sizeof(GLfloat)));

    if (tobj == NULL) {
        tobj = gluNewTess(); /* create and initialize a GLU
                             polygon tesselation object */
        gluTessCallback(tobj, GLU_BEGIN, (void(__stdcall *)(void))glBegin);
        gluTessCallback(tobj, GLU_VERTEX,
                        (void(__stdcall *)(void))glVertex2fv); /* semi-tricky

                                                      */
        gluTessCallback(tobj, GLU_END, glEnd);
    }
    glNewList(side, GL_COMPILE);
    glShadeModel(GL_SMOOTH); /* smooth minimizes seeing
                               tessellation */
    gluBeginPolygon(tobj);
    for (i = 0; i < count; i++) {
        vertex[0] = data[i][0];
        vertex[1] = data[i][1];
        vertex[2] = 0;
        gluTessVertex(tobj, vertex, data[i]);
    }
    gluEndPolygon(tobj);
    glEndList();
    glNewList(edge, GL_COMPILE);
    glShadeModel(GL_FLAT); /* flat shade keeps angular hands
                             from being * * "smoothed" */
    glBegin(GL_QUAD_STRIP);
    for (i = 0; i <= count; i++) {
        /* mod function handles closing the edge */
        glVertex3f(data[i % count][0], data[i % count][1], 0.0);
        glVertex3f(data[i % count][0], data[i % count][1], thickness);
        /* Calculate a unit normal by dividing by Euclidean
           distance. We * could be lazy and use
           glEnable(GL_NORMALIZE) so we could pass in * arbitrary
           normals for a very slight performance hit. */
        dx = data[(i + 1) % count][1] - data[i % count][1];
        dy = data[i % count][0] - data[(i + 1) % count][0];
        len = sqrt(dx * dx + dy * dy);
        glNormal3f(dx / len, dy / len, 0.0);
    }
    glEnd();
    glEndList();
    glNewList(whole, GL_COMPILE);
    glFrontFace(GL_CW);
    glCallList(edge);
    glNormal3f(0.0, 0.0, -1.0); /* constant normal for side */
    glCallList(side);
    glPushMatrix();
    glTranslatef(0.0, 0.0, thickness);
    glFrontFace(GL_CCW);
    glNormal3f(0.0, 0.0, 1.0); /* opposite normal for other side

                               */
    glCallList(side);
    glPopMatrix();
    glEndList();
}

static void
makeDinosaur(void) {
    extrudeSolidFromPolygon(body, sizeof(body), BODY_WIDTH,
                            BODY_SIDE, BODY_EDGE, BODY_WHOLE);
    extrudeSolidFromPolygon(arm, sizeof(arm), BODY_WIDTH / 4,
                            ARM_SIDE, ARM_EDGE, ARM_WHOLE);
    extrudeSolidFromPolygon(leg, sizeof(leg), BODY_WIDTH / 2,
                            LEG_SIDE, LEG_EDGE, LEG_WHOLE);
    extrudeSolidFromPolygon(eye, sizeof(eye), BODY_WIDTH + 0.2,
                            EYE_SIDE, EYE_EDGE, EYE_WHOLE);
    glNewList(DINOSAUR, GL_COMPILE);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, skinColor);
    glCallList(BODY_WHOLE);
    glPushMatrix();
    glTranslatef(0.0, 0.0, BODY_WIDTH);
    glCallList(ARM_WHOLE);
    glCallList(LEG_WHOLE);
    glTranslatef(0.0, 0.0, -BODY_WIDTH - BODY_WIDTH / 4);
    glCallList(ARM_WHOLE);
    glTranslatef(0.0, 0.0, -BODY_WIDTH / 4);
    glCallList(LEG_WHOLE);
    glTranslatef(0.0, 0.0, BODY_WIDTH / 2 - 0.1);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, eyeColor);
    glCallList(EYE_WHOLE);
    glPopMatrix();
    glEndList();
}

static void
recalcModelView(void) {
    GLfloat m[4][4] = {0};
    GLfloat mt[4][4] = {0};
    GLint row = 0;
    GLint col = 0;
    GLint dim = sizeof (m[0])/sizeof(GLfloat);

    controller->process(m);

    // Transpose matrix to match course
    for (row = 0; row < dim; ++row) {
        for (col = 0; col < dim; ++col) {
            mt[col][row] = m[row][col];
        }
    }

    glPopMatrix();
    glPushMatrix();
    glMultMatrixf(&mt[0][0]);
}

static void
redraw(void) {
    recalcModelView();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glCallList(DINOSAUR);
    glutSwapBuffers();
}

static void
reshape(int w, int h) {
    glViewport(0, 0, w, h);
}

void setVisible(int visible) {
    if (visible == GLUT_VISIBLE) {
        glutIdleFunc(glutPostRedisplay);
    } else {
        glutIdleFunc(NULL);
    }
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glutCreateWindow("Tarea 02 - Dinospin");
    glutDisplayFunc(redraw);
    glutReshapeFunc(reshape);
    glutVisibilityFunc(setVisible);

    makeDinosaur();
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glMatrixMode(GL_PROJECTION);
    gluPerspective(40.0, /* field of view in degree */
                   1.0,  /* aspect ratio */
                   1.0,  /* Z near */
                   500.0 /* Z far */
                  );
    glMatrixMode(GL_MODELVIEW);
    gluLookAt(0.0, 0.0, 30.0, /* eye is at (0,0,30) */
              0.0, 0.0, 0.0,  /* center is at (0,0,0) */
              0.0, 1.0, 0.);  /* up is in positive Y direction */

    /* dummy push so we can pop on model recalc */
    glPushMatrix();
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, 1);
    glLightfv(GL_LIGHT0, GL_POSITION, lightZeroPosition);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightZeroColor);
    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.1);
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.05);
    glLightfv(GL_LIGHT1, GL_POSITION, lightOnePosition);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, lightOneColor);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glLineWidth(2.0);

    controller = IController::newController();

    glutMainLoop();

    return 0; /* ANSI C requires main to return int. */
}
