
# Requirements

* OpenCV >= 4.0
* Boost Program Options >= 1.65

# Generating the pattern in Letter paper size

```bash
python $OPENCV_SOURCE_PATH/doc/pattern_tools/gen_pattern.py -o circleboard.svg --rows 4 --columns 5 --type circles --square_size 40 --page_width 215 --page_height 279
```

# Running Calibration

# Aruco Controlled Dinosaur

The Dinosaur will match the Aruco marker on the "Source View" window,
as if it was painted on it.

# Extra

## Setup Clang-Tidy

```bash
sudo apt install bear clang-tidy
cd src
bear make
clang-tidy controller.cpp --checks='*' --fix
cd calibrator
bear make
clang-tidy calibrator.cpp --checks='*' --fix
```